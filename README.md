# Teacode challenge

## Local development
0. Set required node version
1. Install dependencies `yarn install`
2. Copy `.env.example` file to `.env`
3. Run the app by `yarn start`

***Important note:** If you use a default value of `REACT_APP_API_URL` from `.env.example` which is `https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com` then you have to use some browser extension to manipulate CORS handling - otherwise you will not be able to communicate with that API during local development because `https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com` has CORS settings that causes browser to stop you from getting any data.  

## All available scripts
`yarn start` - starts the app for local development
`yarn test` - runs the tests
`yarn dependencies:init` - inits a dependency cruiser, it could be helpful to create new dependency cruiser config from a scratch
`yarn dependencies` - creates a `dependencies.svg` with a graph/diagram of our app dependencies

## Envs
`*REACT_APP_API_URL` - url of an external api to communicate with
`*REACT_APP_MOCKED_DATA` - indicator of should app use real data by communicating with external apis or it should just take a mocked data-set (for situation when we are not able to communicate with an external api/start api locally)

**Legend:**
`*` - means that specified env is required to run the app