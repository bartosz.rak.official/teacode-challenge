import { createSlice, PayloadAction,  } from '@reduxjs/toolkit'
import { User } from '../hooks'

export interface UsersState {
  search: string
  users: User[],
  error: Error | null,
  isLoading: boolean
}
export const usersSlice = createSlice({
  name: 'users',
  initialState: {
    search: "",
    users: [],
    isLoading: false,
    error: null
  } as UsersState,
  reducers: {
    setSearch: (state: UsersState, { payload }: PayloadAction<string>) => {
      state.search = payload
    },
    setUsers: (state: UsersState, { payload }: PayloadAction<User[]>) => {
      state.users = payload
    },
    setError: (state: UsersState, { payload }: PayloadAction<Error | null>) => {
      state.error = payload
    },
    setLoading: (state: UsersState, { payload }: PayloadAction<boolean>) => {
      state.isLoading = payload
    }
  },
})

export const usersActions = usersSlice.actions

export const usersReducer = usersSlice.reducer
