import { makeStyles } from "@material-ui/core";

export const useStyle = makeStyles(() => ({
    root: {
        minHeight: '100vh',
        width: '100%',
    }
}))