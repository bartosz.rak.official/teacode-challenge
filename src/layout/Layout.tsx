import { FunctionComponent } from "react"
import { useStyle } from "./Layout.style"


export const Layout: FunctionComponent= ({ children }) => {
    const classes = useStyle()
    return <div className={classes.root}>
        {children}
    </div>
}