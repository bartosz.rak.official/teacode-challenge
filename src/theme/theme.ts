import { createMuiTheme, responsiveFontSizes } from "@material-ui/core";
import { palette } from './palette'

export const theme = responsiveFontSizes(createMuiTheme({
    palette,
    typography: {
        h1: {
            fontSize: 40
        },
        h2: {
            fontSize: 37
        },
        h3: {
            fontSize: 34
        },
        h4: {
            fontSize: 31
        },
        h5: {
            fontSize: 28
        },
        h6: {
            fontSize: 25
        }
    }
}))