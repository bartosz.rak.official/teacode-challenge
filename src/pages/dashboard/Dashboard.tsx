import { AppBar,  Box,  Typography } from "@material-ui/core"
import { useEffect, VoidFunctionComponent } from "react"
import { useUsers } from "../../hooks"
import { useStyle } from "./Dashboard.style"
import { UserSearch } from "./search/UserSearch"
import { UsersList } from "./users-list/UsersList"


export const Dashboard: VoidFunctionComponent = () => {
    const classes = useStyle()
    const { fetchUsers } = useUsers()

    useEffect(() => {
        fetchUsers()
    }, [fetchUsers])

    return <div className={classes.root}>
        <AppBar className={classes.header} position="relative">
            <Typography variant="h1">
                Contacts
            </Typography>
        </AppBar>
        <Box p={2}>
            <UserSearch />
        </Box>
        <div className={classes.users}>
            <UsersList />
        </div>
    </div>
}