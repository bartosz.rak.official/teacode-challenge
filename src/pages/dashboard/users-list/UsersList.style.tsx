import { makeStyles } from "@material-ui/styles";

export const useStyle = makeStyles(() => ({
    root: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'column'
    },
    footer: {
        display: 'flex',
        justifyContent: 'flex-end',
    }
}))