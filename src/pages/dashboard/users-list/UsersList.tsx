import { Avatar, Box, Button, Checkbox, CircularProgress, List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, Typography } from "@material-ui/core";
import { Add } from "@material-ui/icons";
import { useState, VoidFunctionComponent } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../../state/store";
import { isDefined } from "../../../utils";
import { useStyle } from "./UsersList.style";




export const UsersList: VoidFunctionComponent = () => {
    const classes = useStyle()
    const limitJump = 20
    const [limit, setLimit] = useState(limitJump)
    const [checkedIds, setCheckedIds] = useState<number[]>([])
    const { search, users, isLoading, error } = useSelector((state: RootState) => state.users)

    const handleCheck = (id: number) => {
        const newCheckedIds = [...checkedIds]
        const foundIndex = newCheckedIds.findIndex((checkedId) => checkedId === id)
        if (foundIndex === -1) {
            newCheckedIds.push(id)
        } else {
            newCheckedIds.splice(foundIndex, 1)
        }
        console.log('# All selected contacts:', newCheckedIds)
        setCheckedIds(newCheckedIds)
    }

    if (isLoading) {
        return <Box p={4}>
            <CircularProgress />
        </Box>
    }

    if (isDefined(error)) {
        return <Box>
            <Typography variant="caption" color="error">Error ocurred: ${error.message}</Typography>
        </Box>
    }

    const searchUsers = () => {
        const regex = new RegExp(search, 'gmi')
        const evaluatedUsers = users.map(user => {
            const regexResults = `${user.firstName} ${user.lastName}`.match(regex)
            return {
                ...user,
                evaluation: !isDefined(regexResults) ? 0 : regexResults.length
            }
        })
        const sortedUsers = evaluatedUsers.sort((prevUser, nextUser) => {
            if (prevUser.evaluation < nextUser.evaluation) {
                return 1
            }
            if (prevUser.evaluation > nextUser.evaluation) {
                return -1
            }
            return 0
        })
        return sortedUsers.filter(user => user.evaluation !== 0)
    }

    const preparedUsers = (search === "" ? users : searchUsers()).filter((_, index) => index <= limit)

    if(preparedUsers.length ===0) {
        return <Typography variant="button" color="error">Nothing to display</Typography>
    }

    return <Box className={classes.root}>
        <List>
            {preparedUsers.map(user => {
                const { id, avatar, firstName, lastName, email } = user
                return <ListItem key={id}>
                    <ListItemAvatar>
                        <Avatar src={avatar} />
                    </ListItemAvatar>
                    <ListItemText primary={`${firstName} ${lastName}`} secondary={email} />
                    <ListItemSecondaryAction>
                        <Checkbox checked={checkedIds.includes(id)} onClick={() => handleCheck(id)} />
                    </ListItemSecondaryAction>
                </ListItem>
            })}
        </List>
        <Box p={2} className={classes.footer}>
        <Button variant="contained" color="primary" endIcon={<Add />} onClick={() => setLimit(limit + limitJump)}>Load more</Button>
        </Box>

    </Box>
}