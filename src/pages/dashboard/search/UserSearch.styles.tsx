import { makeStyles } from "@material-ui/core";

export const useStyle = makeStyles(({ palette }) => ({
    textField: {
        backgroundColor: 'white'
    },
    icon: {
        color: palette.grey[600]
    }
}))