import { InputAdornment, TextField } from "@material-ui/core"
import { Search as SearchIcon } from '@material-ui/icons'
import { ChangeEvent, VoidFunctionComponent } from "react"
import { useStyle } from './UserSearch.styles'
import { DebounceInput } from 'react-debounce-input';
import { useDispatch } from "react-redux";
import { usersActions } from "../../../state/users";


export const UserSearch: VoidFunctionComponent = () => {
    const classes = useStyle()
    const dispatch = useDispatch()

    const handleOnChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        dispatch(usersActions.setSearch(event.target.value))
    }

    return <DebounceInput onChange={(event) => handleOnChange(event)} debounceTimeout={1000} element={({ onChange, value }) => (<TextField variant="outlined" color="primary" className={classes.textField} type="text" value={value} onChange={onChange} InputProps={{
        startAdornment: (
            <InputAdornment position="start">
                <SearchIcon className={classes.icon} />
            </InputAdornment>
        )
    }} />)} />
}