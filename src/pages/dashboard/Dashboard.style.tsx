import { makeStyles } from "@material-ui/core";


export const useStyle = makeStyles(({ spacing }) => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
    },
    header: {
        padding: spacing(2),
        textAlign: 'center',
    },
    users: {
        display: 'flex',
        justifyContent: 'center'
    }
}))