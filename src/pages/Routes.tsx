import { VoidFunctionComponent } from "react"
import { Redirect, Route, Switch } from "react-router-dom"
import { Dashboard } from "./dashboard/Dashboard"


export const Routes: VoidFunctionComponent = () => {
    return <Switch>
        <Route path="/" exact>
            <Dashboard />
        </Route>
        <Route path="*">
            <Redirect to="/" />
        </Route>
    </Switch>
}