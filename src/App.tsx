import { CssBaseline, ThemeProvider } from '@material-ui/core';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { Layout } from './layout/Layout';
import { Routes } from './pages/Routes';
import { store } from './state/store';
import { theme } from './theme';

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Layout>
            <Routes />
          </Layout>
        </ThemeProvider>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
