import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

describe("when rendering an App", () => {
  it('returns a proper html structure', () => {
    const renderedApp = render(<App />);
    expect(renderedApp.baseElement).toMatchSnapshot()
  })
});
