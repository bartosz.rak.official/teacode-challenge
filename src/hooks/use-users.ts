import { isLeft } from "fp-ts/lib/Either";
import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { usersActions } from "../state/users";
import { mockedUsers } from "./mocked-users";
import { useConfig } from "./use-config";
import { useHttp } from "./use-http";
import { ApiUser, User } from "./users.definitions";

export const useUsers = (): {
    fetchUsers: () => Promise<void>,
} => {
    const http = useHttp()
    const dispatch = useDispatch()
    const config = useConfig()

    const transformUser = ({ first_name, last_name, ...restOfUser }: ApiUser): User => ({
        firstName: first_name,
        lastName: last_name,
        ...restOfUser,
    })

    const fetchUsers = useCallback(async () => {
        dispatch(usersActions.setLoading(true))
        const transformUsers = (apiUsers: ApiUser[]): Array<User> => {
            const transformedUsers = apiUsers.map(transformUser)
            return transformedUsers.sort(({ lastName: prevLastName }, { lastName: nextLastName }) => {
                if (prevLastName < nextLastName) {
                    return -1
                }
                if (prevLastName > nextLastName) {
                    return 1
                }
                return 0
            })
        }

        if (config.useMockedData) {
            dispatch(usersActions.setUsers(transformUsers(mockedUsers)))
        } else {
            const getUsersResult = await http.fetch<ApiUser[]>('users.json', {
                method: 'GET'
            })
            if (isLeft(getUsersResult)) {
                dispatch(usersActions.setError(getUsersResult.left))
                return
            }
            dispatch(usersActions.setError(null))
            dispatch(usersActions.setUsers(transformUsers(getUsersResult.right)))
        }
        dispatch(usersActions.setLoading(false))
    }, [dispatch, http, config])

    return {
        fetchUsers,
    }
}