export interface User {
    id: number,
    firstName: string,
    lastName: string,
    email: string,
    gender: Gender,
    avatar?: string
}

export enum Gender {
    Male = 'Male',
    Female = 'Female'
}

export interface ApiUser {
    id: number,
    first_name: string,
    last_name: string,
    email: string,
    gender: Gender,
    avatar?: string
}