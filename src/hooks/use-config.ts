import Joi from "joi"
import { isDefined } from "../utils"

interface ConfigEnvs {
    REACT_APP_API_URL: string,
    REACT_APP_MOCKED_DATA: string
}

interface Config {
    apiUrl: string
    useMockedData: boolean
}

const config: Config = {
    apiUrl: process.env.REACT_APP_API_URL || '',
    useMockedData: Boolean(process.env.REACT_APP_MOCKED_DATA)
}

const configValidationSchema: Record<keyof ConfigEnvs, Joi.Schema> = {
    REACT_APP_API_URL: Joi.string().required(),
    REACT_APP_MOCKED_DATA: Joi.number().required()
}

export const checkConfig = () => {
    const schema = Joi.object(configValidationSchema)
    const validationResult = schema.validate(process.env, {
        allowUnknown: true,
        abortEarly: false,
    })
    if(isDefined(validationResult.error)) {
        throw new Error(`Config validation failed! ${validationResult.error.message}`)
    }
}

export const useConfig = (): Config => {
    return config
}