import { Either, left, right } from "fp-ts/lib/Either";
import { isDefined } from "../utils";
import { useConfig } from "./use-config";


export class Http {
    constructor(
        private readonly baseUrl: string,
        private baseHeaders: RequestInit['headers']
    ) { }

    async fetch<T>(path: string, init?: RequestInit): Promise<Either<Error, T>> {
        const validInit: RequestInit = isDefined(init) ? {
            ...init,
            headers: {
                ...init.headers,
                ...this.baseHeaders
            }
        } : {
                headers: this.baseHeaders
            }
        return await this.handleResponse<T>(await fetch(`${this.baseUrl}/${path}`, validInit))
    }

    private async handleResponse<T>(response: Response): Promise<Either<Error, T>> {
        if (!response.ok) {
            return left(new Error('Unknown error ocurred.'))
        }
        try {
            const result: T = await response.json()
            return right(result)
        } catch (err) {
            return left(new Error('Error ocurred during data extraction.'))
        }
    }
}

export const useHttp = (): Http => {
    const config = useConfig()
    return new Http(config.apiUrl, {
        'Content-Type': 'application/json',
    })
}